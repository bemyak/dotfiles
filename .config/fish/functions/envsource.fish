function envsource
  if test (count $argv) -lt 1
    echo "No file specified"
    return 1
  end 
  for line in (cat $argv | grep -v '^#')
    set item (string split -m 1 '=' $line)
    set -gx $item[1] $item[2]
    echo "Exported key $item[1]"
  end
end
