if test -e "$XDG_RUNTIME_DIR/ssh-agent.socket"
	set -x SSH_AUTH_SOCK "$XDG_RUNTIME_DIR/ssh-agent.socket"
	ssh-add
end
