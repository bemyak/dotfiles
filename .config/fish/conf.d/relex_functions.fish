function get_current_path
    set executable "tasks.py"
    set current_path (pwd)
    while not test -e "$current_path/$executable"; and test "$current_path" != "/"
        set current_path (dirname "$current_path")
    end
    echo $current_path
end

function relex
    # Looking for "tasks.py" executable
    set executable "tasks.py"
    set current_path (get_current_path)
    set relex_bin_path ~/.relex_bin

    if not test -e "$current_path/$executable"
        set_color red
        echo "Not run within Relex repository or the repository is too old to contain $executable" >&2
        set_color normal
        return 1
    end

    set python_version_installed (python --version 2>&1 | sed -nE 's/.+[[:blank:]]+([0-9]+).*/\1/p')

    switch (head -n 1 "$current_path/tasks.py")
        case '*python3*'
            set python_version_needed 3
        case '*'
            set python_version_needed 2
    end

    if test "$python_version_installed" != "$python_version_needed"
        set_color yellow
        echo "Python version installed ($python_version_installed) and required ($python_version_needed) do not match, applying hack!" >&2
        set_color normal
        mkdir -p "$relex_bin_path"

        # Set python symlink
        set python_path (command -v python$python_version_needed)
        if test -z "$python_path"
            set_color red
            echo "python$python_version_needed is not found in PATH. Please install it." >&2
            set_color normal
            return 1
        end

        ln -sf "$python_path" "$relex_bin_path/python"

        set python_config_path (command -v python$python_version_needed-config)
        if test -z "$python_config_path"
            set_color red
            echo "python$python_version_needed-config is not found in PATH. Please install it." >&2
            set_color normal
            return 1
        end

        ln -sf "$python_config_path" "$relex_bin_path/python-config"

        set -px PATH "$relex_bin_path"
    end
    python "$current_path/$executable" $argv
end

function with_debug
    set -l help_text "\
Run a command with java debugger properties set. Usage: with_debug [parameters] where parameter is \
-s to suspend JVM on startup, -p port to specify a different port, -h to show this text"

    argparse --name with_debug 'h/help' 's/suspend' 'p/port=!_validate_int --min 1 --max 65535' -- $argv
    or return 1

    if set -q _flag_help
        echo "$help_text"
        return 0
    end

    if test (count $argv) -lt 1
        echo "$help_text"
        return 1
    end

    if not set -q _flag_port
        set _flag_port 5005
    end

    if not set -q _flag_suspend
        set _flag_suspend "n"
    end

    set -ax JAVA_TOOL_OPTIONS "-agentlib:jdwp=transport=dt_socket,server=y,suspend=$_flag_suspend,address=$_flag_port"
    # Running whats left in $argv as command
    $argv

end

function with_jfr
    set -l jfr_directory "$HOME/jfr"
    set -l timestamp (date "+%Y-%m-%d_%H-%M-%S")

    if test -e "$jfr_directory"
        if not test -d "$jfr_directory"
            echo "Trying to write JFR recording to $jfr_directory but it is not a directory"
            return 1
        end
    else
        echo "Creating $jfr_directory and all its parents"
        mkdir -p "$jfr_directory"
    end

    set -l jfr_file_name "$jfr_directory/recording_$timestamp.jfr"
    set -l help_text "\
Run a command with Java Flight Recorder properties set. Usage: with_jfr [parameters] where parameter is \
-s settings_file to specify a settings file for JFR (or 'profile' for JVM's built-in profile settings), -h to show this help"

    set -l jfr_settings_part

    argparse --name with_jfr 'h/help' 's/settings=' -- $argv
    or return 1

    if set -q _flag_help
        echo "$help_text"
        return 0
    end

    if test (count $argv) -lt 1
        echo "$help_text"
        return 1
    end

    if test "$_flag_settings" != "profile"
        if not test -f "$_flag_settings"
            echo "File $_flag_settings not found"
            echo "$help_text"
            return 1
        end

        if not test -r "$_flag_settings"
            echo "File $_flag_settings is not readable"
            echo "$help_text"
            return 1
        end
    end

    set -ax JAVA_TOOL_OPTIONS \
        "-XX:+UnlockDiagnosticVMOptions" \
        "-XX:+DebugNonSafepoints" \
        "-XX:+FlightRecorder" \
        "-XX:StartFlightRecording=dumponexit=true,filename=$jfr_file_name$_flag_settings"

    echo "$JAVA_TOOL_OPTIONS"
    # Running whats left in $argv as command
    $argv

    if test -e "$jfr_file_name$_flag_settings"
        echo "Recording saved to $jfr_file_name$_flag_settings"
    else
        echo "No recording was saved"
    end
end

function with_coverage
    set current_path (get_current_path)

    set -ax JAVA_TOOL_OPTIONS \
        (string join "" -- \
        "-javaagent:$current_path/kernel/processor-test-dependencies/target/org.jacoco.agent-runtime.jar" \
        "=append=true," \
        "includes=fi.relex.*," \
        "destfile=$current_path/kernel/processor-core/target/jacoco/jacoco.exec")

        echo "$JAVA_TOOL_OPTIONS"
        # Running whats left in $argv as command
        $argv
end

function _relex_functions_version
    set -l VERSION 9
    echo $VERSION
end
