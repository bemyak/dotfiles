function gpg-export
	mkdir ~/.gnupg/export
	gpg -a --export-secret-key -o  ~/.gnupg/export/private-key.asc
end

function gpg-import
	gpg --import ~/.gnupg/export/private-key.asc
end

function gpg-ls
	gpg --list-secret-keys --keyid-format LONG
end

function gpg-get
	gpg --armor --export $argv[1]
end

set -x GPG_TTY (tty)
