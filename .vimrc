if &shell =~# 'fish$'
    set shell=sh
endif

syntax on
filetype plugin indent on
set softtabstop=4
set shiftwidth=4
set expandtab
" set noexpandtab
set exrc
set secure
set nocompatible		" be iMproved
set timeoutlen=1000 ttimeoutlen=0
set encoding=utf-8
set number
set clipboard=unnamedplus
highlight LineNr term=bold cterm=NONE ctermfg=DarkGrey ctermbg=NONE gui=NONE guifg=DarkGrey guibg=NONE
cmap w!! w !sudo tee > /dev/null %

"Russian keymap
"set keymap=russian-jcukenwin
"set iminsert=0
"set imsearch=0

highlight lCursor guifg=NONE guibg=Cyan

"fold
set foldmethod=indent
set foldnestmax=10
set nofoldenable
set foldlevel=99

let g:SimpylFold_docstring_preview=1
let g:airline#extensions#tabline#enabled = 1
let g:rustfmt_autosave = 1
let g:elm_detailed_complete = 1
let g:elm_format_autosave = 1
let NERDTreeShowHidden=1
let g:formatdef_rustfmt = '"rustfmt"'
let g:formatters_rust = ['rustfmt']


nmap <silent> <C-Bslash> :NERDTreeToggle<CR>
nmap <F8> :TagbarToggle<CR>


autocmd BufRead *.rs :setlocal tags=./rusty-tags.vi;/,$RUST_SRC_PATH/rusty-tags.vi
autocmd BufWrite *.rs :silent! exec "!rusty-tags vi --quiet --start-dir=" . expand('%:p:h') . "&" <bar> redraw!

" open NERDTree automatically when vim starts up on opening a directory
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | endif
" close vim if the only window left open is a NERDTree
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
"autocmd BufWrite * :Autoformat


if empty(glob('~/.vim/autoload/plug.vim'))
    silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
		\ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.vim/plugged')

Plug 'rust-lang/rust.vim'
Plug 'scrooloose/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'vim-airline/vim-airline'
Plug 'tmhedberg/SimpylFold'
Plug 'w0rp/ale'
Plug 'ElmCast/elm-vim'
Plug 'tpope/vim-fugitive'
Plug 'vim-scripts/ZoomWin'
Plug 'Chiel92/vim-autoformat'
Plug 'bitterjug/vim-tagbar-ctags-elm'
Plug 'majutsushi/tagbar'


call plug#end()
