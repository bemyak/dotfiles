;; General settings
;;(menu-bar-mode -1)
(load-theme 'manoj-dark)
(setq-default word-wrap t)
(setq system-time-locale "C")
(unless (display-graphic-p)
  (xterm-mouse-mode 1))

;; Org-Mode settings
(global-set-key (kbd "C-c l") 'org-store-link)
(global-set-key (kbd "C-c a") 'org-agenda)
(global-set-key (kbd "C-c c") 'org-capture)
(setq org-agenda-files '("~/Notes/"))
(setq org-agenda-start-on-weekday 1)
(setq org-agenda-todo-ignore-scheduled 'future)
(setq-default org-display-custom-times t)
(setq org-time-stamp-custom-formats '("<%d.%m.%Y %a>" . "<%d.%m.%Y %H:%M %a>"))
(setq calendar-date-style "european")
(setq calendar-week-start-day 1)
(setq diary-file "~/Notes/Diary")
(add-hook 'org-mode-hook (lambda () (setq truncate-lines nil)))
(with-eval-after-load "org"
  (define-key org-mode-map (kbd "C-c C-j") #'org-journal-new-entry))

;; load package manager, add the Melpa package registry
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-initialize)

;; bootstrap use-package
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(require 'use-package)

(setq use-package-always-ensure t)

(use-package evil
  :init
  (setq evil-search-module 'evil-search)
  (setq evil-ex-complete-emacs-commands nil)
  (setq evil-vsplit-window-right t)
  (setq evil-split-window-below t)
  (setq evil-shift-round nil)
  (setq evil-want-C-u-scroll nil)
  (setq evil-want-C-i-jump nil)
  (setq evil-undo-system 'undo-fu)
  :config
  (evil-mode)
  (evil-set-initial-state 'calendar-mode 'emacs))

(use-package evil-terminal-cursor-changer
  :config
  (setq etcc-use-blink nil)
  (evil-terminal-cursor-changer-activate))

(use-package org-journal
  :ensure t
  :defer t
  :init
  (setq org-journal-prefix-key "C-c j")
  (setq org-journal-date-format "%d.%m.%Y %a")
  :config
  (setq org-journal-dir "~/Notes/Journal")
  )

(setq truncate-lines nil)
(use-package undo-fu
  :config
  (define-key evil-normal-state-map "u" 'undo-fu-only-undo)
  (define-key evil-normal-state-map "\C-r" 'undo-fu-only-redo))

(use-package which-key
  :config
  (which-key-mode +1))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(inhibit-startup-screen t)
 '(package-selected-packages
   '(evil-terminal-cursor-changer evil which-key use-package undo-fu org-journal)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
